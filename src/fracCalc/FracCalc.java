package fracCalc;

public class FracCalc {

    public static void main(String[] args) {
        Scanner lineOfInput = new Scanner(System.in);
    	System.out.println("Welcome to Fractional Calculator!");
    	System.out.print("Type your equation here : ");
    	String inputEquation = lineOfInput.nextLine();
    	String outputEquation = produceAnswer(inputEquation);
    	System.out.println(outputEquation);
    	
        // TODO: Read the input from the user and call produceAnswer with an equation
        
    }
    
    // ** IMPORTANT ** DO NOT DELETE THIS FUNCTION.  This function will be used to test your code
    // This function takes a String 'input' and produces the result
    //
    // input is a fraction string that needs to be evaluated.  For your program, this will be the user input.
    //      e.g. input ==> "1/2 + 3/4"
    //        
    // The function should return the result of the fraction after it has been calculated
    //      e.g. return ==> "1_1/4"
    public static String produceAnswer(String input {
        String equation = input;
    	int firstSpacePosition = equation.indexOf(" ");
    	int secondSpacePosition = firstSpacePosition + 3;
    	String firstOperand = equation.substring(0, firstSpacePosition);
    	String operator = equation.substring(firstSpacePosition, secondSpacePosition);
    	String secondOperand = equation.substring(secondSpacePosition);
        // TODO: Implement this function to produce the solution to the input
        
        return secondOperand;
    }

    // TODO: Fill in the space below with any helper methods that you think you will need
    
}
